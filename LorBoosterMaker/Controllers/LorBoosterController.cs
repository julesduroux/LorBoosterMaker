﻿using LorBoosterMaker.Domain.Restrictions;
using LorBoosterMaker.Repositories;
using LorBoosterMaker.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LorBoosterMaker.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LorBoosterController : ControllerBase
    {
        private readonly CollectionService _collectionService;
        private readonly RestrictionsRepository _restrictionRepository;
        private readonly PackRepository _packRepository;

        public LorBoosterController(CollectionService collectionService, RestrictionsRepository restrictionRepository, PackRepository packRepository)
        {
            _collectionService = collectionService;
            _restrictionRepository = restrictionRepository;
            _packRepository = packRepository;
        }

        [HttpPost("/cards/pagination")]
        public async Task<ActionResult> GetAsync(CancellationToken cancellationToken, [FromBody] CollectionRequest collectionRequest)
        {
            var re = Request;
            var headers = re.Headers;
            headers.TryGetValue("authorization", out var authorization);
            headers.TryGetValue("magic", out var magic);
            var authToken = authorization.First().Replace("Bearer ", "");
            var result = await _collectionService.GetFilteredPackAsync(collectionRequest, authToken, magic.First(), cancellationToken);
            return Ok(result);
        }

        [HttpPost("/restrictions/General")]
        public ActionResult SetGeneralRestriction([FromBody] GeneralRestriction restriction)
        {
            _restrictionRepository.SetGeneralRestriction(restriction);
            return Ok();
        }

        [HttpPost("/restrictions/CardCostByRegion")]
        public ActionResult SeCardCostByRegionRestriction(IEnumerable<CardCostByRegion> restrictions)
        {
            _restrictionRepository.ReplaceRestrictionsOfType(restrictions);
            return Ok();
        }

        [HttpPost("/restrictions/CardRarityByRegion")]
        public ActionResult SeCardCardRarityByRegionRestriction(IEnumerable<CardRarityByRegion> restrictions)
        {
            _restrictionRepository.ReplaceRestrictionsOfType(restrictions);
            return Ok();
        }

        [HttpPost("/restrictions/CardTypeByRegion")]
        public ActionResult SetCardTypeByRegionRestriction(IEnumerable<CardTypeByRegion> restrictions)
        {
            _restrictionRepository.ReplaceRestrictionsOfType(restrictions);
            return Ok();
        }

        [HttpPost("/clearAll")]
        public ActionResult ClearAll()
        {
            _packRepository.ClearAll();
            return Ok();
        }
    }
}
