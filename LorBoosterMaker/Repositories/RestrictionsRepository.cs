﻿using LorBoosterMaker.Domain.Restrictions;
using System.Collections.Generic;
using System.Linq;

namespace LorBoosterMaker.Repositories
{
    public class RestrictionsRepository
    {
        private readonly List<IRegionRestriction> _restrictions;
        private GeneralRestriction _generalRestriction;

        public RestrictionsRepository(List<IRegionRestriction> restrictions, GeneralRestriction generalRestriction)
        {
            _restrictions = restrictions;
            _generalRestriction = generalRestriction;
        }

        public void ReplaceRestrictionsOfType<TRegionRestriction>(IEnumerable<TRegionRestriction> restrictions) where TRegionRestriction : IRegionRestriction
        {
            var toRemove = _restrictions.Where(r => r.GetType() == typeof(TRegionRestriction));

            _restrictions.RemoveAll(r => toRemove.Contains(r));

            _restrictions.AddRange((IEnumerable<IRegionRestriction>)restrictions);
        }

        public void SetGeneralRestriction(GeneralRestriction restriction)
        {
            _generalRestriction = restriction;
        }

        public IEnumerable<IRegionRestriction> GetAll()
        {
            return _restrictions;
        }

        public GeneralRestriction GetGeneralRestrictions()
        {
            return _generalRestriction;
        }
    }
}
