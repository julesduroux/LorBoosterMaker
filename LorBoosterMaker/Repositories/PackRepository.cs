﻿using LorBoosterMaker.Domain;
using System.Collections.Generic;

namespace LorBoosterMaker.Repositories
{
    public class PackRepository
    {
        private readonly Dictionary<string, Pack> _packsDictionnary;

        public PackRepository()
        {
            _packsDictionnary = new Dictionary<string, Pack>();
        }

        public void Add(Pack pack)
        {
            _packsDictionnary[pack.Id] = pack;
        }

        public Pack? Get(string id)
        {
            _packsDictionnary.TryGetValue(id, out Pack? pack);
            return pack;
        }

        public void ClearAll()
        {
            _packsDictionnary.Clear();
        }
    }
}
