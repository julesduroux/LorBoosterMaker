﻿using LorBoosterMaker.Domain;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LorBoosterMaker.Services
{
    public class CollectionResponse
    {
        public CollectionResponse(IEnumerable<Card> cards)
        {
            Cards = cards;
        }

        [JsonPropertyName("cards")]
        public IEnumerable<Card> Cards { get; set; }
    }
}
