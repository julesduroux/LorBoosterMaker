﻿using LorBoosterMaker.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LorBoosterMaker.Domain
{
    public class Pack
    {
        public Pack(string id)
        {
            Id = id;
        }

        public string Id { get; set; }
        public List<Card> Cards { get; set; } = new List<Card>();

        public void AddCard(Card card)
        {
            Cards.Add(card);
        }

        public void AddCards(IEnumerable<Card> cards)
        {
            Cards.AddRange(cards);
        }

        internal IEnumerable<Card> GetFitered(Filters filters)
        {
            IEnumerable<Card> result = Cards;
            if (filters.Cost is not null && filters.Cost.Count > 0)
            {
                result = result.Where(c => filters.Cost.Contains(c.Cost));
            }
            if (filters.RegionRef is not null && filters.RegionRef.Count > 0)
            {
                result = result.Where(c => filters.RegionRef.Contains(c.RegionRef));
            }
            if (filters.Type is not null && filters.Type.Count > 0)
            {
                result = result.Where(c => filters.Type.Contains(c.Type));
            }
            if (filters.RarityRef is not null && filters.RarityRef.Count > 0)
            {
                result = result.Where(c => filters.RarityRef.Contains(c.RarityRef));
            }
            if (filters.Set is not null && filters.Set.Count > 0)
            {
                result = result.Where(c => filters.Set.Select(s => $"Set{s}").Contains(c.Set));
            }
            if (filters.SuperTypes is not null && filters.SuperTypes.Count > 0)
            {
                result = result.Where(c => filters.SuperTypes.Contains(c.SuperType));
            }

            return result;
        }
    }
}
