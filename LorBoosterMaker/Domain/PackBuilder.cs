﻿using LorBoosterMaker.Domain.Restrictions;
using LorBoosterMaker.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LorBoosterMaker.Domain
{
    public class PackBuilder
    {
        private readonly RestrictionsRepository _restrictionsRepository;

        public PackBuilder(RestrictionsRepository restrictionsRepository)
        {
            _restrictionsRepository = restrictionsRepository;
        }

        public Pack BuildPack(string id, IEnumerable<Card> cards)
        {
            //We group cards by region and shuffle the regions
            var cardsGroupByRegion = cards.GroupBy(g => g.RegionRef).Select(grp => new { region = grp.Key, cards = grp.ToList() }).OrderBy(c => Guid.NewGuid()).ToList();

            var generalRestrictions = _restrictionsRepository.GetGeneralRestrictions();

            var pack = new Pack(id);
            var completedRegions = 0;
            foreach (var region in cardsGroupByRegion)
            {
                var regionBuilder = new RegionBuilder(_restrictionsRepository.GetAll(), generalRestrictions.CardsByRegion);

                var secondPassage = new List<Card>();
                foreach (var card in DissociateCopiesAndShuffle(region.cards))
                {
                    if (regionBuilder.TryAddCard(card, false))
                    {
                        if (regionBuilder.IsComplete)
                        {
                            completedRegions++;
                            pack.AddCards(AssociateCopiesAndOrder(regionBuilder.Cards));
                            break;
                        }
                    }
                    else
                    {
                        //Ajouter la carte comme eligible au second passage
                        secondPassage.Add(card);
                    }
                }

                //Second passage en autorisant les cartes qui ne matchent pas
                foreach (var card in secondPassage)
                {
                    if (regionBuilder.TryAddCard(card, true) && regionBuilder.IsComplete)
                    {
                        completedRegions++;
                        pack.AddCards(AssociateCopiesAndOrder(regionBuilder.Cards));
                        break;
                    }
                }

                if (completedRegions >= generalRestrictions.Regions)
                {
                    break;
                }
            }

            return pack;
        }

        public static IEnumerable<Card> DissociateCopiesAndShuffle(IEnumerable<Card> cards)
        {
            var result = new List<Card>();
            foreach (var card in cards)
            {
                int maxVail = card.NbAvailable;
                card.NbAvailable = 1;
                for (int i = 0; i < maxVail; i++)
                {
                    result.Add(card);
                }
            }
            return result.OrderByDescending(c => c.RarityRef == "Champion").ThenBy(c => Guid.NewGuid());
        }

        public static IEnumerable<Card> AssociateCopiesAndOrder(IEnumerable<Card> cards)
        {
            var result = new List<Card>();
            var groups = cards.GroupBy(c => c.CardCode).Select(g => new { first = g.First(), count = g.Count() });
            foreach (var group in groups)
            {
                var card = group.first;
                card.NbAvailable = group.count;
                result.Add(card);
            }
            return result.OrderBy(c => c.RegionRef).ThenByDescending(c => c.RarityRef == "Champion").ThenBy(c => c.Type).ThenBy(c => c.Cost);
        }
    }
}
