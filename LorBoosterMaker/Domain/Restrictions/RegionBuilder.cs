﻿using System;
using System.Collections.Generic;

namespace LorBoosterMaker.Domain.Restrictions
{
    public class RegionBuilder
    {
        private readonly List<RestrictionRegionWatcher> _watchers;
        private int _reservedSlots;
        public int Size { get; }
        public List<Card> Cards { get; }
        public bool IsComplete => Cards.Count == Size;

        public RegionBuilder(IEnumerable<IRegionRestriction> restrictions, int size)
        {
            var watchers = new List<RestrictionRegionWatcher>();
            _reservedSlots = 0;
            foreach (var restriction in restrictions)
            {
                var watcher = new RestrictionRegionWatcher(restriction);
                watchers.Add(watcher);
                _reservedSlots += watcher.ReservedSlot;
            }
            _watchers = watchers;
            Size = size;
            Cards = new List<Card>();
        }

        public bool TryAddCard(Card card, bool allowOtherCards)
        {
            if (Cards.Count >= Size)
            {
                return false;
            }
            var resevedSlotMatched = 0;
            foreach (var watcher in _watchers)
            {
                if (watcher.DoesCardMatch(card))
                {
                    if (watcher.ReservedSlot > 0)
                    {
                        resevedSlotMatched++;
                    }
                    else if (watcher.HasReachedMax)
                    {
                        return false;
                    }
                }
            }

            if(resevedSlotMatched > 0 || (Size - Cards.Count - _reservedSlots > 0 && allowOtherCards))
            {
                AddCard(card);
                _reservedSlots = Math.Max(_reservedSlots - resevedSlotMatched, 0);
                return true;
            }
            return false;

        }

        private void AddCard(Card card)
        {
            foreach (var watcher in _watchers)
            {
                watcher.AddCard(card);
            }
            Cards.Add(card);
        }
    }
}
