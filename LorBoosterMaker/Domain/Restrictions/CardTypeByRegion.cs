﻿namespace LorBoosterMaker.Domain.Restrictions
{
    public class CardTypeByRegion : IRegionRestriction
    {
        public CardTypeByRegion(string type, int? min, int? max)
        {
            Type = type;
            Min = min;
            Max = max;
        }

        public string Type { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }

        public bool Match(Card card)
        {
            return card.Type == Type;
        }
    }
}
