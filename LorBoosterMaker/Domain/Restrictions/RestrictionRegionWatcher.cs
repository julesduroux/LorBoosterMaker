﻿using System;

namespace LorBoosterMaker.Domain.Restrictions
{
    public class RestrictionRegionWatcher
    {
        private readonly IRegionRestriction _restriction;
        private int _matchingCards;

        public RestrictionRegionWatcher(IRegionRestriction restriction)
        {
            _restriction = restriction;
            _matchingCards = 0;
        }

        public int ReservedSlot => _restriction.Min is null ? 0 : Math.Max(_restriction.Min.Value - _matchingCards, 0);
        public bool HasReachedMax => _restriction.Max is not null && _matchingCards >= _restriction.Max.Value;

        public bool DoesCardMatch(Card card)
        {
            return _restriction.Match(card);
        }

        public void AddCard(Card card)
        {
            if (DoesCardMatch(card))
            {
                _matchingCards++;
            }
        }
    }
}
